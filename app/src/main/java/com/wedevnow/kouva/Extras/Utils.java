package com.wedevnow.kouva.Extras;

import com.google.android.gms.common.util.Strings;

public class Utils {
    public static String toUpCaseFirst(String t){
        return t.substring(0,1).toUpperCase()+t.substring(1);
    }
}