package com.wedevnow.kouva.Models.Users;


import com.google.firebase.Timestamp;

public class UserWishListModel {
    private String state;
    private Timestamp createdAt;

    public UserWishListModel(String state, Timestamp createdAt) {
        this.state = state;
        this.createdAt = createdAt;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public UserWishListModel() {
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
