package com.wedevnow.kouva.Models.Homes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class GMSplace implements Parcelable {
    private Double longitude;
    private Double latitude;
    private String formatted_address;
    private String place_id;

    public GMSplace(Double longitude, Double latitude, String formatted_address, String place_id, String name) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.formatted_address = formatted_address;
        this.place_id = place_id;
        this.name = name;
    }

    public GMSplace() {
    }

    protected GMSplace(Parcel in) {
        if (in.readByte() == 0) {
            longitude = null;
        } else {
            longitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            latitude = null;
        } else {
            latitude = in.readDouble();
        }
        formatted_address = in.readString();
        place_id = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (longitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(longitude);
        }
        if (latitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(latitude);
        }
        dest.writeString(formatted_address);
        dest.writeString(place_id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GMSplace> CREATOR = new Creator<GMSplace>() {
        @Override
        public GMSplace createFromParcel(Parcel in) {
            return new GMSplace(in);
        }

        @Override
        public GMSplace[] newArray(int size) {
            return new GMSplace[size];
        }
    };

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
