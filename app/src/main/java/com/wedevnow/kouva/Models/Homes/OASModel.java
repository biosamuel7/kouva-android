package com.wedevnow.kouva.Models.Homes;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class OASModel {
    private String possessionDuBien;
    private String typeDuBien;

    public OASModel() {
    }

    public OASModel(String possessionDuBien, String typeDuBien) {
        this.possessionDuBien = possessionDuBien;
        this.typeDuBien = typeDuBien;
    }

    public String getPossessionDuBien() {
        return possessionDuBien;
    }

    public void setPossessionDuBien(String possessionDuBien) {
        this.possessionDuBien = possessionDuBien;
    }

    public String getTypeDuBien() {
        return typeDuBien;
    }

    public void setTypeDuBien(String typeDuBien) {
        this.typeDuBien = typeDuBien;
    }
}
