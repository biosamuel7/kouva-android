package com.wedevnow.kouva.Models.Homes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.Timestamp;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;

@IgnoreExtraProperties
public class HomeModel implements Parcelable {

    private Integer douches;
    private Integer chambres;
    private String commune;
    private String description;
    private Integer salons;
    private ImageModel imagePrincipale;
    private ArrayList<ImageModel> imageGallerie;
    private GMSplace place;
    private Integer pieces;
    private String prix;
    private String etage;
    private String superficie;
    private String quartier;
    private String type;
    private String numero;
    private String possession;
    private String homeID;
    private Boolean isLike = false;
    private String likeState;
    private Boolean disponibilite;
    private String pasDePorte;
    private Timestamp createdAt;

    public HomeModel() {
    }

    public HomeModel(Integer douches, Integer chambres, String commune, String description, Integer salons, ImageModel imagePrincipale, ArrayList<ImageModel> imageGallerie, GMSplace place, Integer pieces, String prix, String etage, String superficie, String quartier, String type, String numero, String possession, String homeID, Boolean isLike, String likeState, Boolean disponibilite, String pasDePorte, Timestamp createdAt) {
        this.douches = douches;
        this.chambres = chambres;
        this.commune = commune;
        this.description = description;
        this.salons = salons;
        this.imagePrincipale = imagePrincipale;
        this.imageGallerie = imageGallerie;
        this.place = place;
        this.pieces = pieces;
        this.prix = prix;
        this.etage = etage;
        this.superficie = superficie;
        this.quartier = quartier;
        this.type = type;
        this.numero = numero;
        this.possession = possession;
        this.homeID = homeID;
        this.isLike = isLike;
        this.likeState = likeState;
        this.disponibilite = disponibilite;
        this.pasDePorte = pasDePorte;
        this.createdAt = createdAt;
    }

    protected HomeModel(Parcel in) {
        if (in.readByte() == 0) {
            douches = null;
        } else {
            douches = in.readInt();
        }
        if (in.readByte() == 0) {
            chambres = null;
        } else {
            chambres = in.readInt();
        }
        commune = in.readString();
        description = in.readString();
        if (in.readByte() == 0) {
            salons = null;
        } else {
            salons = in.readInt();
        }
        imagePrincipale = in.readParcelable(ImageModel.class.getClassLoader());
        imageGallerie = in.createTypedArrayList(ImageModel.CREATOR);
        place = in.readParcelable(GMSplace.class.getClassLoader());
        if (in.readByte() == 0) {
            pieces = null;
        } else {
            pieces = in.readInt();
        }
        prix = in.readString();
        etage = in.readString();
        superficie = in.readString();
        quartier = in.readString();
        type = in.readString();
        numero = in.readString();
        possession = in.readString();
        homeID = in.readString();
        byte tmpIsLike = in.readByte();
        isLike = tmpIsLike == 0 ? null : tmpIsLike == 1;
        likeState = in.readString();
        byte tmpDisponibilite = in.readByte();
        disponibilite = tmpDisponibilite == 0 ? null : tmpDisponibilite == 1;
        pasDePorte = in.readString();
        createdAt = in.readParcelable(Timestamp.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (douches == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(douches);
        }
        if (chambres == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(chambres);
        }
        dest.writeString(commune);
        dest.writeString(description);
        if (salons == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(salons);
        }
        dest.writeParcelable(imagePrincipale, flags);
        dest.writeTypedList(imageGallerie);
        dest.writeParcelable(place, flags);
        if (pieces == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(pieces);
        }
        dest.writeString(prix);
        dest.writeString(etage);
        dest.writeString(superficie);
        dest.writeString(quartier);
        dest.writeString(type);
        dest.writeString(numero);
        dest.writeString(possession);
        dest.writeString(homeID);
        dest.writeByte((byte) (isLike == null ? 0 : isLike ? 1 : 2));
        dest.writeString(likeState);
        dest.writeByte((byte) (disponibilite == null ? 0 : disponibilite ? 1 : 2));
        dest.writeString(pasDePorte);
        dest.writeParcelable(createdAt, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomeModel> CREATOR = new Creator<HomeModel>() {
        @Override
        public HomeModel createFromParcel(Parcel in) {
            return new HomeModel(in);
        }

        @Override
        public HomeModel[] newArray(int size) {
            return new HomeModel[size];
        }
    };

    public Integer getDouches() {
        return douches;
    }

    public void setDouches(Integer douches) {
        this.douches = douches;
    }

    public Integer getChambres() {
        return chambres;
    }

    public void setChambres(Integer chambres) {
        this.chambres = chambres;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSalons() {
        return salons;
    }

    public void setSalons(Integer salons) {
        this.salons = salons;
    }

    public ImageModel getImagePrincipale() {
        return imagePrincipale;
    }

    public void setImagePrincipale(ImageModel imagePrincipale) {
        this.imagePrincipale = imagePrincipale;
    }

    public ArrayList<ImageModel> getImageGallerie() {
        return imageGallerie;
    }

    public void setImageGallerie(ArrayList<ImageModel> imageGallerie) {
        this.imageGallerie = imageGallerie;
    }

    public GMSplace getPlace() {
        return place;
    }

    public void setPlace(GMSplace place) {
        this.place = place;
    }

    public Integer getPieces() {
        return pieces;
    }

    public void setPieces(Integer pieces) {
        this.pieces = pieces;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    public String getEtage() {
        return etage;
    }

    public void setEtage(String etage) {
        this.etage = etage;
    }

    public String getSuperficie() {
        return superficie;
    }

    public void setSuperficie(String superficie) {
        this.superficie = superficie;
    }

    public String getQuartier() {
        return quartier;
    }

    public void setQuartier(String quartier) {
        this.quartier = quartier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getPossession() {
        return possession;
    }

    public void setPossession(String possession) {
        this.possession = possession;
    }

    public String getHomeID() {
        return homeID;
    }

    public void setHomeID(String homeID) {
        this.homeID = homeID;
    }

    public Boolean getLike() {
        return isLike;
    }

    public void setLike(Boolean like) {
        isLike = like;
    }

    public String getLikeState() {
        return likeState;
    }

    public void setLikeState(String likeState) {
        this.likeState = likeState;
    }

    public Boolean getDisponibilite() {
        return disponibilite;
    }

    public void setDisponibilite(Boolean disponibilite) {
        this.disponibilite = disponibilite;
    }

    public String getPasDePorte() {
        return pasDePorte;
    }

    public void setPasDePorte(String pasDePorte) {
        this.pasDePorte = pasDePorte;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }
}
