package com.wedevnow.kouva.Models;

public class TypeDeBienModel {
    private String typeDebien;
    private int icon;
    private boolean isSelected;

    public void setTypeDebien(String typeDebien) {
        this.typeDebien = typeDebien;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }

    public String getTypeDebien() {
        return typeDebien;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
    public TypeDeBienModel(){

    }
}
