package com.wedevnow.kouva.Models.Users;

public class SearchBudget {
    private Integer min;
    private Integer max;

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public String getMin_max(){
        return  this.min.toString() +" - "+ this.max.toString();
    }


    public SearchBudget(Integer min, Integer max) {
        this.min = min;
        this.max = max;
    }

    public SearchBudget() {
    }
}
