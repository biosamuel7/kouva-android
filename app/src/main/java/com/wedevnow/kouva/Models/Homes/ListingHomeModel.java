package com.wedevnow.kouva.Models.Homes;

import java.util.ArrayList;

public class ListingHomeModel {
    private Integer Column;
    private ArrayList<HomeModel> homes;

    public Integer getColumn() {
        return Column;
    }

    public void setColumn(Integer column) {
        Column = column;
    }

    public ArrayList<HomeModel> getHomes() {
        return homes;
    }

    public void setHomes(ArrayList<HomeModel> homes) {
        this.homes = homes;
    }

    public ListingHomeModel(Integer column, ArrayList<HomeModel> homes) {
        Column = column;
        this.homes = homes;
    }
}
