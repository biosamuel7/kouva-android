package com.wedevnow.kouva.Models.Homes;

public class NombreDePieceModel {
    private int piece;
    private boolean isSelected = false;

    public NombreDePieceModel(int piece, boolean isSelected) {
        this.piece = piece;
        this.isSelected = isSelected;
    }

    public int getPiece() {
        return piece;
    }

    public void setPiece(int piece) {
        this.piece = piece;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
