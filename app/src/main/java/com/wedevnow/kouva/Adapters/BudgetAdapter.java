package com.wedevnow.kouva.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.wedevnow.kouva.Models.Users.SearchBudget;
import com.wedevnow.kouva.R;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class BudgetAdapter extends BaseAdapter {
    private ArrayList<SearchBudget> searchBudgets;
    private LayoutInflater inflater;
    private Context context;

    public BudgetAdapter(Context context, ArrayList<SearchBudget> datas) {
        this.context = context;
        this.searchBudgets = datas ;
        this.inflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return searchBudgets.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = inflater.inflate(R.layout.adapter_budget_alerte,null);
        TextView textView = (TextView) row.findViewById(R.id.budget_alerte_txt);
        textView.setText(searchBudgets.get(position).getMin_max());
        return  row;
    }
}
