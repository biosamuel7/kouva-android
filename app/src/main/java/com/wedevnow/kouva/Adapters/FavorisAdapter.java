package com.wedevnow.kouva.Adapters;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.wedevnow.kouva.Activities.ShowHomeDetailsActivity;
import com.wedevnow.kouva.Fragments.FavorisFragment;
import com.wedevnow.kouva.Models.Homes.HomeModel;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.DataBaseService;

import java.util.ArrayList;

public class FavorisAdapter extends RecyclerView.Adapter<FavorisAdapter.MyViewHolder> {
    Context context;
    ArrayList<HomeModel> data;
    LayoutInflater inflater;

    public FavorisAdapter(Context context, ArrayList<HomeModel> data) {

        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.adapter_favoris, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        HomeModel home = data.get(position);
        myViewHolder.SetImage(home.getImagePrincipale().getPath());
        myViewHolder.SetInfoTextView(home);

        if (!home.getDisponibilite()){
            myViewHolder.homeTakenView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        private final Context context;
        FirebaseStorage storage = FirebaseStorage.getInstance();
        private TextView price_tv,localite_tv,piece_tv;
        private Button likeBtn;
        private RelativeLayout homeTakenView;

        public MyViewHolder(View itemView) {
            super(itemView);

            homeTakenView = (RelativeLayout) itemView.findViewById(R.id.homeTakenView_id);
            imageView = (ImageView) itemView.findViewById(R.id.imgHome);
            context = itemView.getContext();

            likeBtn = (Button) itemView.findViewById(R.id.likeButton);
            price_tv = (TextView)itemView.findViewById(R.id.id_price);
            piece_tv = (TextView) itemView.findViewById(R.id.id_nombreDePiece);
            localite_tv = (TextView) itemView.findViewById(R.id.id_localite);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Intent intent =  new Intent(context, ShowHomeDetailsActivity.class);
                    intent.putExtra("homeData",data.get(position));

                    Boolean result = data.get(position).getDisponibilite();
                    if (result){
                        context.startActivity(intent);
                    }

                }
            });

            likeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HandleLikeHome((Button)view,getAdapterPosition());
                }

            });
        }

        public void SetInfoTextView(HomeModel homeModel){
            switch (homeModel.getPossession()){
                case "Location":
                    int result = "Studio".compareTo(homeModel.getType());
                    if(result == 0){
                        piece_tv.setText("Studio à louer");
                    } else {

                        piece_tv.setText(homeModel.getType()+" "+homeModel.getPieces()+"pieces à louer");
                    }
                    break;
                case "Vente":

                    piece_tv.setText(homeModel.getType()+" "+homeModel.getPieces()+" pieces à Vendre");
            }
            localite_tv.setText(homeModel.getCommune()+" - "+homeModel.getQuartier());
            price_tv.setText(homeModel.getPrix()+" FCFA");


            switch (homeModel.getLikeState()){
                case "wish":
                    likeBtn.setBackground(context.getDrawable(R.mipmap.likehome_wish_icon));
                    break;
                case "visite":
                    likeBtn.setBackground(context.getDrawable(R.mipmap.likehome_visite_icon));
                    break;
                default:break;
            }

        }
        public void HandleLikeHome(final Button btn, final int position){
            final HomeModel homeModel = data.get(position);
            if(homeModel.getLike()){
                DataBaseService.userWishCollection(context).document(homeModel.getHomeID()).delete();
                notifyDataSetChanged();
            }
        }


        public void SetImage(String path){
            StorageReference storageReference = storage.getReference(path);
            Glide.with(context.getApplicationContext())
                    .load(storageReference)
                    .into(imageView);
        }
    }
}
