package com.wedevnow.kouva.Adapters.UserAdapters;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wedevnow.kouva.Activities.UserActivities.LoginActivity;
import com.wedevnow.kouva.Activities.UserActivities.AproposActivity;
import com.wedevnow.kouva.Activities.UserActivities.NousContacterActivity;
import com.wedevnow.kouva.Activities.UserActivities.TermeEtConditionActivity;
import com.wedevnow.kouva.Fragments.UserFragments.UserProfileFragment;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.AuthService;

public class UserProfileAdapter extends RecyclerView.Adapter<UserProfileAdapter.MyViewHolder> {
    FragmentActivity activity;
    LayoutInflater inflater;

    public UserProfileAdapter(FragmentActivity context){

        this.activity= context;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public UserProfileAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.adapter_user_profil,parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(UserProfileAdapter.MyViewHolder holder, int position) {
        switch (position) {
            case 0:
            //    holder.imageView.setImageResource(R.mipmap.agrement_icon);
                holder.txtProfile.setText("Termes et conditions");
                break;
            case 1:
             //   holder.imageView.setImageResource(R.mipmap.aboutus_icons);
                holder.txtProfile.setText("A propos de Kouva");
                break;
            case 2:
              //  holder.imageView.setImageResource(R.mipmap.contacter);
                holder.txtProfile.setText("Nous contacter");
                break;
            case 3:
              //  holder.imageView.setImageResource(R.mipmap.remarque_icon);
                holder.txtProfile.setText("Partager");
                break;
            case 4:
              //  holder.imageView.setVisibility(View.GONE);
                if(AuthService.CheckIfUserIsConnected(activity)){
                    holder.txtProfile.setText("Deconnexion");
                }else {
                    holder.txtProfile.setText("Connexion");
                }
                break;
            default: break;

        }

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    //    ImageView imageView;
        TextView txtProfile;
        final Context context;


        public MyViewHolder(View itemView) {
            super(itemView);
//            imageView = (ImageView) itemView.findViewById(R.id.txt_icon);
            txtProfile =  (TextView) itemView.findViewById(R.id.txt_profile);
            context = itemView.getContext();

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (getAdapterPosition()){

                case 0:
                    Intent intentTerme = new Intent(context.getApplicationContext(), TermeEtConditionActivity.class);
                    activity.startActivity(intentTerme);
                    break;

                case 1:
                    Intent intentApropos = new Intent(context.getApplicationContext() , AproposActivity.class);
                    activity.startActivity(intentApropos);
                    break;

                case 2:
                    Intent intentContacts = new Intent(context.getApplicationContext() , NousContacterActivity.class);
                    activity.startActivity(intentContacts);
                    break;

                case 3:
                    Intent sharedAppIntent = new Intent(Intent.ACTION_SEND);
                    sharedAppIntent.putExtra(Intent.EXTRA_TEXT, "https://kouva.page.link/m6ab");
                    sharedAppIntent.setType("text/plain");
                    activity.startActivity(Intent.createChooser(sharedAppIntent, "Share Link"));
                    break;


                case 4: // CONNEXION OR DECONNEXION
                    if(AuthService.CheckIfUserIsConnected(activity)){
                        AuthService.logout(activity);
                        ((AppCompatActivity)context).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.main_container, new UserProfileFragment())
                                .commit();
                    }else {
                        Intent intent =  new Intent(context.getApplicationContext(), LoginActivity.class);
                        activity.startActivityForResult(intent,0);
                    }
                    break;
            }
        }
    }
}
