package com.wedevnow.kouva.Adapters;

import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wedevnow.kouva.Activities.ListingHomesActivity;
import com.wedevnow.kouva.Models.TypeDeBienModel;
import com.wedevnow.kouva.R;

import java.util.ArrayList;

public class TypeDeBienAdapter extends RecyclerView.Adapter<TypeDeBienAdapter.MyViewHolder> {

    Context context;
    ArrayList<TypeDeBienModel> data;
    LayoutInflater inflater;
    OnItemClickListener mListener;
    String possessionDuBien;


    public TypeDeBienAdapter(Context context, ArrayList<TypeDeBienModel> data, String possessionDuBien) {

        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
        this.possessionDuBien = possessionDuBien;

    }


    @Override
    public TypeDeBienAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.adapter_type_de_bien, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(final TypeDeBienAdapter.MyViewHolder holder, int position) {
        holder.tv_item_name.setText(data.get(position).getTypeDebien());
        holder.image_main.setImageResource(data.get(position).getIcon());

        if (data.get(position).isSelected()) {
            holder.image_main.setImageResource(R.mipmap.newspaper_active);
            holder.background.setBackgroundResource(R.drawable.primary_color_corner);
            holder.tv_item_name.setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
            holder.background.setBackgroundResource(R.drawable.normal_background);
            holder.tv_item_name.setTextColor(ContextCompat.getColor(context, R.color.black_alpha_40));
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_item_name;
        ImageView image_main;
        View tapInterceptor;
        final Context context;
        RelativeLayout background;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_item_name = (TextView) itemView.findViewById(R.id.tv_item_name);
            image_main = (ImageView) itemView.findViewById(R.id.image_main);
            background = (RelativeLayout) itemView.findViewById(R.id.activity_main);

            tapInterceptor = itemView.findViewById(R.id.tap_interceptor);
            context = itemView.getContext();
            tv_item_name.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            for (TypeDeBienModel info : data) {
                info.setSelected(false);
            }
            data.get(getAdapterPosition()).setSelected(true);
            Intent intent = new Intent(context, ListingHomesActivity.class);
            intent.putExtra("possession",possessionDuBien.toLowerCase());
            intent.putExtra("type",data.get(getAdapterPosition()).getTypeDebien().toLowerCase());
            context.startActivity(intent);
            notifyDataSetChanged();

        }


    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    public interface OnItemClickListener {
        void onClick(TypeDeBienModel item);
    }
}
