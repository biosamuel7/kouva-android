package com.wedevnow.kouva.Adapters;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wedevnow.kouva.Activities.ListingHomesActivity;
import com.wedevnow.kouva.Models.Homes.NombreDePieceModel;
import com.wedevnow.kouva.R;

import java.util.ArrayList;

public class NombreDePiecesAdapter extends RecyclerView.Adapter<NombreDePiecesAdapter.MyViewHolder> {
    ListingHomesActivity context;
    ArrayList<NombreDePieceModel> data;
    LayoutInflater inflater;


    public NombreDePiecesAdapter(ListingHomesActivity context, ArrayList<NombreDePieceModel> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public NombreDePiecesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.adapter_nombre_de_piece, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(final MyViewHolder myViewHolder, int position) {

        myViewHolder.textView.setText(String.valueOf(data.get(position).getPiece()));

        if (data.get(position).isSelected()) {
            myViewHolder.background.setBackgroundResource(R.drawable.primary_color_corner);
            myViewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
            myViewHolder.background.setBackgroundResource(R.drawable.bg_pieces);
            myViewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.primaryColor));
        }


    }
    public void selectePiece(int position){
        for (NombreDePieceModel item : data) {
            item.setSelected(false);
        }
        data.get(position).setSelected(true);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return data == null ? 5 : data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        RelativeLayout background;
        TextView textView;
        public MyViewHolder(View itemView) {
            super(itemView);

            background = (RelativeLayout) itemView.findViewById(R.id.todo);
            textView = (TextView) itemView.findViewById(R.id.tv_item_nb);
            background.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            for (NombreDePieceModel item : data) {
                item.setSelected(false);
            }
            data.get(getAdapterPosition()).setSelected(true);
            context.mRecyclerView_listings.scrollToPosition(getAdapterPosition());
            notifyDataSetChanged();

        }

    }
}
