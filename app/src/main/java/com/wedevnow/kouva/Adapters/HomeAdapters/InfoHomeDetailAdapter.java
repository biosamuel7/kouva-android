package com.wedevnow.kouva.Adapters.HomeAdapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wedevnow.kouva.Models.Homes.HomeModel;
import com.wedevnow.kouva.R;

public class InfoHomeDetailAdapter extends RecyclerView.Adapter<InfoHomeDetailAdapter.InfoParallaxViewHolder> {

    private Context mContext;
    private HomeModel homeData;

    public InfoHomeDetailAdapter(Context context, HomeModel home){
        mContext = context;
        this.homeData = home;
    }

    @Override
    public InfoHomeDetailAdapter.InfoParallaxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InfoParallaxViewHolder(LayoutInflater.from(mContext).inflate(R.layout.adapter_info_villa_detail, parent, false));
    }


    @Override
    public void onBindViewHolder(InfoHomeDetailAdapter.InfoParallaxViewHolder holder, int position) {
        switch (position){
            case 0:
                holder.tvCount.setText(String.valueOf(homeData.getChambres()));
                holder.imgIcon.setImageResource(R.mipmap.chambre);
                holder.tvTitle.setText("CHAMBRE");
                break;
            case 1:

                Integer result = homeData.getType().compareTo("appartement");
                if (result == 0 ) {
                    holder.tvCount.setText(String.valueOf(homeData.getDouches()));
                    holder.imgIcon.setImageResource(R.mipmap.douche);
                    holder.tvTitle.setText("DOUCHE");
                } else  {
                    holder.tvCount.setText("1");
                    holder.imgIcon.setImageResource(R.mipmap.cuisine);
                    holder.tvTitle.setText("CUISINE");
                }

                break;
            case 2: holder.tvCount.setText(String.valueOf(homeData.getDouches()));
                holder.imgIcon.setImageResource(R.mipmap.douche);
                holder.tvTitle.setText("DOUCHE");
                break;
            case 3: holder.tvCount.setText(String.valueOf(homeData.getSalons()));
                holder.imgIcon.setImageResource(R.mipmap.salon);
                holder.tvTitle.setText("SALON");
                break;
                default: break;
        }
    }

    @Override
    public int getItemCount() {
        Integer result = homeData.getType().compareTo("appartement");
        return (result == 0 ) ? 2 : 4;
    }

    public class InfoParallaxViewHolder extends RecyclerView.ViewHolder {

        ImageView imgIcon;
        TextView tvTitle, tvCount;

        public InfoParallaxViewHolder(View itemView) {
            super(itemView);

            imgIcon = (ImageView) itemView.findViewById(R.id.img_icon);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvCount = (TextView) itemView.findViewById(R.id.tv_count);
        }
    }
}
