package com.wedevnow.kouva.Adapters.HomeAdapters;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.wedevnow.kouva.Extras.GlideApp;
import com.wedevnow.kouva.Models.Homes.ImageModel;
import com.wedevnow.kouva.R;

import java.util.ArrayList;

public class ImageSlideAdapter extends PagerAdapter {

    public ArrayList<ImageModel> images;
    Context context;
    LayoutInflater layoutInflater;

    FirebaseStorage storage = FirebaseStorage.getInstance();


    public ImageSlideAdapter(Context context, ArrayList<ImageModel> galleriList){
        this.context=context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.images = galleriList;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = layoutInflater.inflate(R.layout.image_slide_adapter, container, false);

        ImageView imageview = (ImageView) itemView.findViewById(R.id.image_swipe);
        StorageReference storageReference = storage.getReference(images.get(position).getPath());
        GlideApp.with(context.getApplicationContext() /* context */)
                .load(storageReference)
                .into(imageview);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (LinearLayout)object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
