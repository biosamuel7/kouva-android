package com.wedevnow.kouva.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wedevnow.kouva.Activities.ListingHomesActivity;
import com.wedevnow.kouva.Models.Homes.HomeModel;
import com.wedevnow.kouva.Models.Homes.ListingHomeModel;
import com.wedevnow.kouva.R;

import java.util.ArrayList;

public class ListingHomesBySectionAdapter extends RecyclerView.Adapter<ListingHomesBySectionAdapter.MyViewHolder> {
    Context context;
    LayoutInflater inflater;
    ArrayList<ListingHomeModel> homelistings;
    public ListingHomesBySectionAdapter(ListingHomesActivity context, ArrayList<ListingHomeModel> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.homelistings = data;
    }

    @Override

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.adapter_listing_homes_by_section, parent, false);
        ListingHomesBySectionAdapter.MyViewHolder holder = new ListingHomesBySectionAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.showSecteur(homelistings.get(position).getHomes());
    }


    @Override
    public int getItemCount() {
        return homelistings == null ? 0 : homelistings.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RecyclerView itemRecycleView;
        public MyViewHolder(View itemView) {
            super(itemView);

            itemRecycleView = (RecyclerView)itemView.findViewById(R.id.recycler_view_listinghome_by_item);
        }

        public void showSecteur(ArrayList<HomeModel> data){
            itemRecycleView.setHasFixedSize(true);
            itemRecycleView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
            itemRecycleView.setAdapter(new ListingHomeByItemAdapter(context,data));
        }
    }
}
