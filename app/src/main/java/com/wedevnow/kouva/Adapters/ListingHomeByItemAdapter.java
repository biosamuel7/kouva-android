package com.wedevnow.kouva.Adapters;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.Timestamp;
import com.google.firebase.storage.FirebaseStorage;
import com.wedevnow.kouva.Activities.ShowHomeDetailsActivity;
import com.wedevnow.kouva.Extras.GlideApp;
import com.wedevnow.kouva.Activities.UserActivities.LoginActivity;
import com.wedevnow.kouva.Extras.Utils;
import com.wedevnow.kouva.Models.Homes.HomeModel;
import com.wedevnow.kouva.Models.Users.UserWishListModel;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.AuthService;
import com.wedevnow.kouva.Services.DataBaseService;

import java.util.ArrayList;
import java.util.Date;

public class ListingHomeByItemAdapter extends RecyclerView.Adapter<ListingHomeByItemAdapter.MyViewHolder> {
    Context context;
    ArrayList<HomeModel> data;
    LayoutInflater inflater;


    public ListingHomeByItemAdapter(Context context, ArrayList<HomeModel> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.adapter_listing_home_by_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        myViewHolder.setItemInformation(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageView;
        private final Context context;
        private TextView priceTV, localityTV, pieceTV;
        private Button likeBtn;

        public MyViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();

            imageView = (ImageView) itemView.findViewById(R.id.img_listing);
            imageView.setOnClickListener(this);

            priceTV = (TextView) itemView.findViewById(R.id.id_price);
            localityTV = (TextView) itemView.findViewById(R.id.id_localite);
            pieceTV = (TextView) itemView.findViewById(R.id.id_nombreDePiece);

            likeBtn = (Button) itemView.findViewById(R.id.likeButton);
            likeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HandleLikeHome((Button)view,getAdapterPosition());
                }

            });
        }
        //set listing home informations
        public void setItemInformation(HomeModel home){
            DataBaseService.CheckIfHomeIsLiked(context,home,likeBtn);
            GlideApp.with(context.getApplicationContext() /* context */)
                    .load(FirebaseStorage.getInstance().getReference(home.getImagePrincipale().getPath()))
                    .into(imageView);

            priceTV.setText(home.getPrix()+" FCFA");
            localityTV.setText(home.getCommune()+" - "+home.getQuartier());
            switch (home.getType()){
                case "Magasin":
                    pieceTV.setText(home.getPossession().compareTo("location") == 0 ? Utils.toUpCaseFirst(home.getType()) + " à louer": home.getType()+ " à vendre");
                case "Studio":
                    pieceTV.setText(home.getPossession().compareTo("location") == 0 ? Utils.toUpCaseFirst(home.getType())+ " à louer": home.getType()+ " à vendre");
                default:
                    pieceTV.setText(home.getPossession().compareTo("location") == 0 ? Utils.toUpCaseFirst(home.getType())+" "+home.getPieces()+" pièces à louer" : home.getType()+" "+home.getPieces()+" pièces à vendre");
            }
        }


        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ShowHomeDetailsActivity.class);
            intent.putExtra("homeID",data.get(getAdapterPosition()).getHomeID());
            context.startActivity(intent);
        }

        //todo: handle wish list retrieve
        public void HandleLikeHome(final Button btn, int position){
            if(AuthService.CheckIfUserIsConnected(context)){
                final HomeModel homeModel = data.get(position);
                if(homeModel.getLike()){
                    DataBaseService.userWishCollection(context).document(homeModel.getHomeID()).delete();
                    btn.setBackgroundResource(R.mipmap.unlike);
                    homeModel.setLike(false);
                } else {
                    homeModel.setLike(true);
                    homeModel.setLikeState("wish");
                    UserWishListModel wishListModel = new UserWishListModel("wish",new Timestamp(new Date()));
                    DataBaseService.userWishCollection(context).document(homeModel.getHomeID()).set(wishListModel);
                    btn.setBackgroundResource(R.mipmap.likehome_wish_icon);
                }
            }else {
                Intent intent; intent =  new Intent(context.getApplicationContext(), LoginActivity.class);
                context.startActivity(intent);
            }
        }
    }
}
