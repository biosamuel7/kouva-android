package com.wedevnow.kouva.Adapters.HomeAdapters;

import android.content.Context;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.wedevnow.kouva.Extras.GlideApp;
import com.wedevnow.kouva.Fragments.HomeFragments.GalleryFragment;
import com.wedevnow.kouva.Models.Homes.ImageModel;
import com.wedevnow.kouva.R;

import java.util.ArrayList;

public class InfoGalleryAdapter extends RecyclerView.Adapter<InfoGalleryAdapter.MyViewHolder> {
    GalleryFragment context;
    ArrayList<ImageModel> data;
    LayoutInflater inflater;
    Bundle bundle = new Bundle();


    public InfoGalleryAdapter (GalleryFragment context ,ArrayList<ImageModel> list){

        this.context= context;
        this.data = list;
        inflater = LayoutInflater.from(context.getActivity());

    }


    @Override
    public InfoGalleryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.from(parent.getContext()).inflate(R.layout.adapter_info_gallery,parent,false);
        MyViewHolder holder = new MyViewHolder(view,context,data);
        return holder;
    }

    @Override
    public void onBindViewHolder(InfoGalleryAdapter.MyViewHolder holder, int position) {

        holder.SetImage(data.get(position).getPath());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        FirebaseStorage storage = FirebaseStorage.getInstance();
        final Context context;

        public MyViewHolder(View itemView, GalleryFragment context, ArrayList<ImageModel> data) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.img_row1);
            this.context = itemView.getContext();
        }

        public void SetImage(String path){
            StorageReference storageReference = storage.getReference(path);

            GlideApp.with(context.getApplicationContext() /* context */)
                    .load(storageReference)
                    .into(imageView);

        }

    }
}
