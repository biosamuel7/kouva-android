package com.wedevnow.kouva.Services;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.auth.FirebaseAuth;

public class AuthService {
    public static FirebaseAuth auth(){
        FirebaseAuth.getInstance().useAppLanguage();
        return FirebaseAuth.getInstance();
    }

    public static String getUserID(Context context){
        return getUserInfo(context,"userID");
    }
    public static Boolean CheckIfUserIsConnected(Context context){
        String value = context.getSharedPreferences("ID", Context.MODE_PRIVATE).getString("userID",null);
        return  value != null;
    }
    public static void saveUserInfo(Context context, String key, String value){
        SharedPreferences preferences = context.getSharedPreferences("ID", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key,value);
        editor.apply();
    }

    public static String getUserInfo(Context context, String key) {
        return context.getSharedPreferences("ID",Context.MODE_PRIVATE).getString(key,null);
    }

    public static void logout(Context context){
        FirebaseAuth.getInstance().signOut();
        SharedPreferences preferences = context.getSharedPreferences("ID", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}
