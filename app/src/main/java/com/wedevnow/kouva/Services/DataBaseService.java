package com.wedevnow.kouva.Services;

import android.content.Context;
import android.widget.Button;

import com.google.firebase.Timestamp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.wedevnow.kouva.Models.Homes.HomeModel;
import com.wedevnow.kouva.Models.Users.UserWishListModel;
import com.wedevnow.kouva.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class DataBaseService {


    public static DocumentReference userDocument(Context context) {
        return FirebaseFirestore.getInstance().collection("users").document(AuthService.getUserID(context));
    }

    public static CollectionReference userCollection() {
        return  FirebaseFirestore.getInstance().collection("users");
    }

    public static CollectionReference homeCollection() {
        return FirebaseFirestore.getInstance().collection("homes");
    }

    public static CollectionReference userWishCollection(Context context){
        return userDocument(context).collection("wishlist");
    }

    public static CollectionReference userAlerteCollection(Context context){
        return userDocument(context).collection("alerte");
    }

    public static CollectionReference userSearchCollection(Context context) {
        return userDocument(context).collection("user-search");
    }

    public static CollectionReference OASCollection(){
        return FirebaseFirestore.getInstance().collection("oas");
    }

    public static void setUserLike(Context context,HomeModel homeModel){
        userWishCollection(context).document(homeModel.getHomeID()).set(new UserWishListModel("wish",new Timestamp(new Date())));
    }

    public static void setUserVisite(Context context,HomeModel homeModel){
        Map<String, Object> docData = new HashMap<>();
        docData.put("homeId", homeModel.getHomeID());
        docData.put("userId", AuthService.getUserID(context));
        docData.put("createdAt", new Timestamp(new Date()));
        FirebaseFirestore.getInstance().collection("calls").add(docData);
    }

    public static void CheckIfHomeIsLiked(Context context, final HomeModel homeModel, final Button homeBtn){
        if(AuthService.CheckIfUserIsConnected(context)) {
            userWishCollection(context).document(homeModel.getHomeID()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                    if (documentSnapshot != null && documentSnapshot.exists()){
                        UserWishListModel wishListModel = documentSnapshot.toObject(UserWishListModel.class);
                        homeModel.setLike(true);
                        homeModel.setLikeState(wishListModel.getState());
                        homeBtn.setBackgroundResource(R.mipmap.likehome_wish_icon);

                    } else {
                        homeBtn.setBackgroundResource(R.mipmap.unlike);
                    }
                }
            });
        }

    }
}
