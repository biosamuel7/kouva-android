package com.wedevnow.kouva.Services;

import android.net.Uri;
import androidx.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

public class DynamicLinksService {
    public static Uri generateContentLink() {
        Uri baseUrl = Uri.parse("https://kouva.net/links/share/8Ng4");
        String domain = "https://kouva.net/links/share";

        DynamicLink link = FirebaseDynamicLinks.getInstance()
                .createDynamicLink()
                .setLink(baseUrl)
                .setDomainUriPrefix(domain)
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.wedevnow.kouva").build())
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder("com.wedevnow.kouva").build())
                .buildDynamicLink();

        return link.getUri();
    }

    public static Uri generateContentLinkShortLink() {
        Uri baseUrl = Uri.parse("https://kouva.net/");
        String domain = "https://kouva.net/links/share";

        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(baseUrl)
                .setDomainUriPrefix(domain)
                .buildShortDynamicLink()
                .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Log.v("debug shortLink", shortLink.toString());
                            Uri flowchartLink = task.getResult().getPreviewLink();
                        } else {
                            // Error
                            // ...
                        }
                    }
                });

        return shortLinkTask.getResult().getShortLink();
    }


}
