package com.wedevnow.kouva.Services;

public class Constants {
    public static final String BASE_URL = "https://kouva.net/"; // url roots
    public static final String TERMS_URL = BASE_URL + "terms"; // url show all terms and app policies
}
