package com.wedevnow.kouva.Fragments;


import android.content.Context;
import android.os.Bundle;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wedevnow.kouva.R;

/**
 * A simple {@link Fragment} subclass.
 */
 public class BottomSheetBudgetFragment extends BottomSheetDialogFragment {

    private BottomSheetListener mListener;


    public BottomSheetBudgetFragment getInstance() {
        return new BottomSheetBudgetFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View v = inflater.inflate(R.layout.bottom_sheet_budget, container, false);

//         RadioButton radioButton = v.findViewById(R.id.radioBtn1);



  //      radioButton.setOnClickListener(new View.OnClickListener() {
    //        @Override
      //      public void onClick(View view) {
//                mListener.onButtonClicked("BudgetClicked");
        //        mListener.onButtonClicked("yes");
          //      dismiss();

            //}
        //});
        return v;

    }



    public interface BottomSheetListener{
        void onButtonClicked(String text);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof BottomSheetListener) {
            try {
                mListener = (BottomSheetListener) context;
            }
            catch (ClassCastException e){
                throw new ClassCastException(context.toString()
                        + " must implement BottomsheetListener");
            }
        }
    }

}
