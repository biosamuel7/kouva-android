package com.wedevnow.kouva.Fragments.AuthFragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;

import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.AuthService;
import com.wedevnow.kouva.Services.DataBaseService;

/**
 * A simple {@link Fragment} subclass.
 */
public class VerificationUserNumberFragment extends Fragment {

    private ViewPager viewPager;
    private int resultCode;
    private Button checkCodeBtn;
    private ProgressBar progressBar;
    private Pinview pinview;

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
    }


    public VerificationUserNumberFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_verification_user_number,container,false);

      //  Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbarVerificationNumber);
        //toolbar.setNavigationIcon(R.mipmap.deleteview_icon);

        pinview = (Pinview) v.findViewById(R.id.pinview);
        checkCodeBtn = (Button) v.findViewById(R.id.checkCodeSms_btn_id);
        progressBar = (ProgressBar) v.findViewById(R.id.verify_progressBar_id);

        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                //Make api calls here or what not
                String code = pinview.getValue();
                VerifyUserByPhoneNumber(code);
            }
        });


        return v;
    }
    /*
     * Verify if the code recieve is the good
     */
    public void VerifyUserByPhoneNumber(String code) {
        progressBar.setVisibility(View.VISIBLE);
        checkCodeBtn.setText(" ");
        String verificationId = AuthService.getUserInfo(getContext(),"verificationId");
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId,code);
        signInWithPhoneAuthCredential(credential);
    }
    /*
     * SignIn with credential get by PhoneAuth after user verified her number phone
     */
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("debug", "signInWithCredential:success");
                    Log.v("debug user Id",task.getResult().getUser().getUid());

                    progressBar.setVisibility(View.GONE);
                    ((InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(pinview.getWindowToken(),0);
                    String userPhoneNumber = AuthService.getUserInfo(getContext(),"userPhoneNumber");
                    DataBaseService.userDocument(getContext()).update("numero",userPhoneNumber);
                    Intent intent = new Intent();

                    //Todo: dismiss keyboard before left activity
                    getActivity().setResult(resultCode,intent);
                    getActivity().finish();
                }
            }
        });
    }
}
