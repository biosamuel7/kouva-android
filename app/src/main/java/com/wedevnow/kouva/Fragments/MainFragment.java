package com.wedevnow.kouva.Fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.wedevnow.kouva.Adapters.OpportunityAdapter;
import com.wedevnow.kouva.Adapters.TypeDeBienAdapter;
import com.wedevnow.kouva.Datas.DataTypeDeBien;
import com.wedevnow.kouva.Datas.DataTypeDeBienAchat;
import com.wedevnow.kouva.Models.Homes.HomeModel;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.DataBaseService;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    RecyclerView mRecyclerview_opportunite;
    OpportunityAdapter opportunityAdapter;
    private ShimmerFrameLayout mShimmerViewContainer;
    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_main, container, false);

        final RecyclerView mRecyclerView_location = (RecyclerView) v.findViewById(R.id.recyclerview_location);
        final RecyclerView mRecyclerView_vente = (RecyclerView) v.findViewById(R.id.recyclerview_vente);
         mRecyclerview_opportunite = (RecyclerView) v.findViewById(R.id.recyclerview_opportunites);

        new RetrieveOASHomeDataTask().execute();

        mRecyclerView_location.setHasFixedSize(true);
        mRecyclerView_vente.setHasFixedSize(true);
        mRecyclerview_opportunite.setHasFixedSize(true);

        //set the adapter
        mRecyclerView_location.setAdapter(new TypeDeBienAdapter(getActivity(), DataTypeDeBien.getData(),"location"));
        mRecyclerView_vente.setAdapter(new TypeDeBienAdapter(getActivity(), DataTypeDeBienAchat.getData(),"vente"));


        mRecyclerView_location.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView_vente.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


        mShimmerViewContainer = v.findViewById(R.id.shimmer_view_container);


        return v;


    }

    private class RetrieveOASHomeDataTask extends AsyncTask<Void, Void, ArrayList<HomeModel>> {

        ArrayList<HomeModel> dataOASHomesListing = new ArrayList<>();
        @Override
        protected ArrayList<HomeModel> doInBackground(Void... voids) {
            DataBaseService.OASCollection().get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful() && task.getResult().size() != 0){
                                for (DocumentSnapshot document : task.getResult().getDocuments()){
                                    DataBaseService.homeCollection()
                                            .document(document.getId())
                                            .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> taskHome) {
                                            if (taskHome.isSuccessful() && taskHome.getResult().exists()){
                                                HomeModel home = taskHome.getResult().toObject(HomeModel.class);
                                                home.setHomeID(taskHome.getResult().getId());
                                                //retreive only home available
                                                if (home.getDisponibilite()) {
                                                    mShimmerViewContainer.setVisibility(View.GONE);
                                                    dataOASHomesListing.add(home);
                                                    mRecyclerview_opportunite.invalidate();
                                                    opportunityAdapter.notifyDataSetChanged();
                                                }
                                            }
                                    }});
                                }
                            } else {
                                mShimmerViewContainer.setVisibility(View.GONE);
                            }
                        }
                    });

            return dataOASHomesListing;
        }

        @Override
        protected void onPostExecute(ArrayList<HomeModel> dataHomes) {
            opportunityAdapter = new OpportunityAdapter(getContext(),dataHomes);
            mRecyclerview_opportunite.setAdapter(opportunityAdapter);
            mRecyclerview_opportunite.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }




}
