package com.wedevnow.kouva.Fragments.HomeFragments;


import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.wedevnow.kouva.Activities.ShowHomeDetailsActivity;
import com.wedevnow.kouva.Adapters.HomeAdapters.InfoHomeDetailAdapter;
import com.wedevnow.kouva.Models.Homes.GMSplace;
import com.wedevnow.kouva.Models.Homes.HomeModel;
import com.wedevnow.kouva.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class VillaDetailFragment extends Fragment implements OnMapReadyCallback {


    public HomeModel HomeData;

    public HomeModel getHomeData() {
        return HomeData;
    }

    public void setHomeData(HomeModel homeData) {
        HomeData = homeData;
    }

    public VillaDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_villa_detail, container, false);

        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        mRecyclerView.setAdapter( new InfoHomeDetailAdapter(getActivity(),HomeData));

        ReadMoreTextView DescriptionTextView = (ReadMoreTextView)view.findViewById(R.id.id_description);
        TextView dimensionTextView = (TextView)view.findViewById(R.id.id_dimension);

        TextView timePublicationTextView = (TextView) view.findViewById(R.id.date_publication_id);
        ((ShowHomeDetailsActivity) getActivity()).disPlayDateTimeAge(timePublicationTextView);


        DescriptionTextView.setText(HomeData.getDescription());
        DescriptionTextView.setTrimCollapsedText("Plus");
        DescriptionTextView.setTrimExpandedText("");

        dimensionTextView.setText("une villa de "+HomeData.getSuperficie());

        MapFragment mapFragment = (MapFragment) getActivity().getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        GMSplace place = HomeData.getPlace();
        LatLng latLng = new LatLng(place.getLatitude(), place.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng,15);
        googleMap.moveCamera(cameraUpdate);
        googleMap.setMaxZoomPreference(15);
        googleMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(300)
                .strokeColor(R.color.primaryColor)
                .fillColor(Color.argb(39,141,130,206)));
    }
}
