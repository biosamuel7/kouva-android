package com.wedevnow.kouva.Fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.wedevnow.kouva.Adapters.FavorisAdapter;
import com.wedevnow.kouva.Models.Homes.HomeModel;
import com.wedevnow.kouva.Models.Users.UserWishListModel;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.AuthService;
import com.wedevnow.kouva.Services.DataBaseService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.annotation.Nullable;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavorisFragment extends Fragment {


    public ProgressBar progressBar;
  //  Button mAddMaison;
    RecyclerView recyclerViewFavoris;
    FavorisAdapter favorisAdapter;
    String btnState;

    public FavorisFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favoris, container, false);

        recyclerViewFavoris = (RecyclerView) view.findViewById(R.id.recyclerviewFavoris);
//        mAddMaison = (Button) view.findViewById(R.id.buttonAddFavoris);
        progressBar = (ProgressBar) view.findViewById(R.id.homeProgress);

        if(AuthService.CheckIfUserIsConnected(getActivity())){
            progressBar.setVisibility(View.VISIBLE);
           new FavorisQueryTask().execute();
        }

    //    mAddMaison.setTextColor(getActivity().getResources().getColor(R.color.colorWish));
      //  mAddMaison.setBackgroundTintList(null);
        //mAddMaison.setBackground(getActivity().getDrawable(R.drawable.input_out2));
        //mAddMaison.setText("Ajouter une maison");

        /*mAddMaison.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).getBottomNavigationView().setSelectedItemId(R.id.id_recherche);
            }
        });

        */


        return view;
    }

    public class FavorisQueryTask extends AsyncTask<Void,Void, ArrayList<HomeModel>> {

        @Override
        protected ArrayList<HomeModel> doInBackground(Void... voids) {

            final ArrayList<HomeModel> list = new ArrayList<>();

            DataBaseService.userWishCollection(getContext()).addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                    if (queryDocumentSnapshots.size() != 0){
                        list.clear();
                        for (DocumentChange documentChange : queryDocumentSnapshots.getDocumentChanges()){
                            Log.v("wish",documentChange.getDocument().getData().toString());
                            Log.v("wish",documentChange.getDocument().getId());
                            final UserWishListModel wish = documentChange.getDocument().toObject(UserWishListModel.class);
                            if (wish.getState().compareTo("wish") == 0){
                                DataBaseService.homeCollection()
                                        .document(documentChange.getDocument().getId())
                                        .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful() && task.getResult().exists()){
                                            HomeModel homeModel = task.getResult().toObject(HomeModel.class);
                                            homeModel.setLike(true);
                                            homeModel.setLikeState(wish.getState());
                                            homeModel.setHomeID(task.getResult().getId());

                                            Collections.sort(list, new Comparator<HomeModel>() {
                                                @Override
                                                public int compare(HomeModel homeModel, HomeModel t1) {
                                                    return homeModel.getHomeID().compareTo(t1.getHomeID());
                                                }
                                            });

                                            //delete repeating homes
                                            for(int i = 0; i < list.size(); i++){
                                                if(list.get(i).getHomeID() == homeModel.getHomeID()){
                                                    list.remove(i);
                                                }
                                            }
                                            if (homeModel.getDisponibilite()){
                                                list.add(homeModel);
                                            } else {
                                                DataBaseService.userWishCollection(getContext()).document(homeModel.getHomeID()).delete();
                                            }

                                            progressBar.setVisibility(View.GONE);
                                            recyclerViewFavoris.invalidate();
                                            favorisAdapter.notifyDataSetChanged();
                                        }
                                    }
                                });
                            }
                        }

                    } else {
                        progressBar.setVisibility(View.GONE);
                        list.clear();
                    }
                }
            });
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<HomeModel> homeList) {
            favorisAdapter = new FavorisAdapter(getContext() , homeList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
            recyclerViewFavoris.setLayoutManager(linearLayoutManager);
            recyclerViewFavoris.setAdapter(favorisAdapter);
        }
    }


}
