package com.wedevnow.kouva.Fragments.AuthFragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.AuthService;
import com.wedevnow.kouva.Services.DataBaseService;

import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddUserNumberPhoneFragment extends Fragment {

    private ViewPager viewPager;
    private int resultCode;

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    private ProgressBar progressBar;
    private Button verifyBtn;
    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
    }


    public AddUserNumberPhoneFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_add_user_number_phone,container,false);

//        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbarAddingNumber);
     //   toolbar.setNavigationIcon(R.mipmap.deleteview_icon);

        progressBar = (ProgressBar) v.findViewById(R.id.verify_progressBar_id);
        verifyBtn = (Button) v.findViewById(R.id.verify_btn);

        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = ((EditText) v.findViewById(R.id.id_phone_number_EditText)).getText().toString();
                sendCodeVerificationBySms(phoneNumber);
            }
        });

        return v;
    }

    /*
     * Send code to user by sms
     */
    public void sendCodeVerificationBySms(final String phoneNumber) {
        progressBar.setVisibility(View.VISIBLE);
        verifyBtn.setText(" ");
        FirebaseAuth.getInstance().setLanguageCode("fr");
        PhoneAuthProvider.getInstance().verifyPhoneNumber("+225"+phoneNumber, 60, TimeUnit.SECONDS, getActivity(), new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.v("debug completed",credential.toString());
                AuthService.saveUserInfo(getContext(),"userPhoneNumber","+225"+phoneNumber);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                progressBar.setVisibility(View.GONE);
                verifyBtn.setText("Verifier");
                Log.i("debug",e.getLocalizedMessage());
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
//                super.onCodeSent(verificationId, forceResendingToken);
                Log.v("debug verificationId",verificationId);
                Log.v("debug token",forceResendingToken.toString());
                AuthService.saveUserInfo(getContext(),"verificationId",verificationId);
                AuthService.saveUserInfo(getContext(),"userPhoneNumber","+225"+phoneNumber);
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                progressBar.setVisibility(View.GONE);
                verifyBtn.setText("Verifier");
            }
        });
    }
    /*
     * SignIn with credential get by PhoneAuth after user verified her number phone
     */
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {

        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("debug", "signInWithCredential:success");
                    Log.v("debug user Id",task.getResult().getUser().getUid());
                    String userPhoneNumber = AuthService.getUserInfo(getContext(),"userPhoneNumber");
                    DataBaseService.userDocument(getContext()).update("numero",userPhoneNumber);

                    progressBar.setVisibility(View.GONE);

                    Intent intent = new Intent();
                    getActivity().setResult(resultCode,intent);
                    getActivity().finish();
                }
            }
        });
    }

}
