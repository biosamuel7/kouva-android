package com.wedevnow.kouva.Fragments.AuthFragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.wedevnow.kouva.Models.Users.UserModel;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.AuthService;
import com.wedevnow.kouva.Services.DataBaseService;

import io.fabric.sdk.android.Fabric;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    Button mRegisterBtn;
    private EditText mEmailEditText, mPasswordEditText, mNameEditText,mPrenomTxt;
    TextView mDisplaypassword;
    ProgressBar progressBar;
    Boolean checkstate = true ;

    private ViewPager viewPager;

    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    public RegisterFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register,container,false);
        Fabric.with(getActivity(), new Crashlytics[]{new Crashlytics()});

        //Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbarRegisterActivity);
        //toolbar.setNavigationIcon(R.mipmap.deleteview_icon);


        progressBar = (ProgressBar) v.findViewById(R.id.progression);
        mNameEditText = (EditText) v.findViewById(R.id.id_name_register);
        mEmailEditText = (EditText) v.findViewById(R.id.id_email_register);
        mPasswordEditText = (EditText) v.findViewById(R.id.id_password_register);
        mPrenomTxt = (EditText) v.findViewById(R.id.id_prenom_register);


        mDisplaypassword = (TextView) v.findViewById(R.id.afficher_password_register) ;

        mDisplaypassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (checkstate == true) {
                    mDisplaypassword.setText("Masquer");
                    mPasswordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    checkstate = false;

                } else {
                    mDisplaypassword.setText("Afficher");
                    mPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    checkstate = true;
                }

            }

        });

        // hide keyboard

        mEmailEditText.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
        mNameEditText.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
        mPrenomTxt.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
        mPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);


        mPasswordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        mRegisterBtn = (Button) v.findViewById(R.id.buttonRegister);
        mRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HandleInscription(view);
            }
        });

        return v;

    }

    public void HandleInscription(final View view){
        if(mNameEditText.getText().length() == 0 || mPrenomTxt.getText().length() == 0 || mEmailEditText.getText().length() == 0  || mPasswordEditText.getText().length() == 0){
            Snackbar.make(view,"Remplir les champs vides",Snackbar.LENGTH_LONG)
                    .setAction("Action",null).show();
        }else {
            progressBar.setVisibility(View.VISIBLE);
            mRegisterBtn.setText(" ");
            AuthService.auth().createUserWithEmailAndPassword(mEmailEditText.getText().toString(),mPasswordEditText.getText().toString())
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull final Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                AuthService.saveUserInfo(getContext(),"userID",task.getResult().getUser().getUid());
                                FirebaseUser user = task.getResult().getUser();
                                String token = AuthService.getUserInfo(getContext(),"token");
                                DataBaseService.userCollection().document(user.getUid()).set(new UserModel(mNameEditText.getText().toString(),mPrenomTxt.getText().toString(),user.getEmail(),token));
                                DataBaseService.userDocument(getContext()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()){
                                            AuthService.saveUserInfo(getContext(),"userName",mPrenomTxt.getText().toString()+" "+mNameEditText.getText().toString());
                                            AuthService.saveUserInfo(getContext(),"userEmail",mEmailEditText.getText().toString());
//                                                    AuthService.auth().getCurrentUser().sendEmailVerification();
                                            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                                            progressBar.setVisibility(View.GONE);
                                            mRegisterBtn.setText("Valider");
                                        }
                                    }
                                });
                            }else {
                                progressBar.setVisibility(View.GONE);
                                mRegisterBtn.setText("Valider");
                                Snackbar.make(view,task.getException().getLocalizedMessage(),Snackbar.LENGTH_LONG)
                                        .setAction("Action",null).show();
                        }
                    }
            });

        }

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("code 2",String.valueOf(resultCode));
        if(resultCode == 2){
            Intent intent = new Intent();
            getActivity().setResult(1,intent);
            getActivity().finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                //onBackPressed();
                return true;
            }
            default:{
                return super.onOptionsItemSelected(item);
            }
        }
    }

    // hide keyboard method

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
