package com.wedevnow.kouva.Fragments;


import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.wedevnow.kouva.Adapters.FavorisAdapter;
import com.wedevnow.kouva.Adapters.ListingHomeByItemAdapter;
import com.wedevnow.kouva.Models.Homes.HomeModel;
import com.wedevnow.kouva.Models.Users.UserWishListModel;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.AuthService;
import com.wedevnow.kouva.Services.DataBaseService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.annotation.Nullable;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlertesFragment extends Fragment {

    RecyclerView recyclerViewAlerte;
    ListingHomeByItemAdapter listingHomeByItemAdapters;
    ProgressBar progressBar;


    public  AlertesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_alerte, container, false);

        recyclerViewAlerte = (RecyclerView) v.findViewById(R.id.recyclerviewAlerte);

        progressBar = (ProgressBar) v.findViewById(R.id.homeProgress);

        if(AuthService.CheckIfUserIsConnected(getActivity())){
            progressBar.setVisibility(View.VISIBLE);
            new AlerteQueryTask().execute();
        }

        return v;
    }


    public class AlerteQueryTask extends AsyncTask<Void,Void, ArrayList<HomeModel>> {

        @Override
        protected ArrayList<HomeModel> doInBackground(Void... voids) {

            final ArrayList<HomeModel> list = new ArrayList<>();

            DataBaseService.userAlerteCollection(getContext()).addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                    if (queryDocumentSnapshots.size() != 0){
                        list.clear();
                        for (DocumentChange documentChange : queryDocumentSnapshots.getDocumentChanges()){
                                DataBaseService.homeCollection()
                                        .document(documentChange.getDocument().getId())
                                        .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful() && task.getResult().exists()){
                                            HomeModel homeModel = task.getResult().toObject(HomeModel.class);
                                            homeModel.setHomeID(task.getResult().getId());

                                            Collections.sort(list, new Comparator<HomeModel>() {
                                                @Override
                                                public int compare(HomeModel homeModel, HomeModel t1) {
                                                    return homeModel.getHomeID().compareTo(t1.getHomeID());
                                                }
                                            });

                                            if (homeModel.getDisponibilite()){
                                                list.add(homeModel);
                                            }

                                            progressBar.setVisibility(View.GONE);
                                            recyclerViewAlerte.invalidate();
                                            listingHomeByItemAdapters.notifyDataSetChanged();
                                        }
                                    }
                                });
                            }

                    } else {
                        progressBar.setVisibility(View.GONE);
                        list.clear();
                    }
                }
            });
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<HomeModel> homeList) {
            listingHomeByItemAdapters = new ListingHomeByItemAdapter(getContext() , homeList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
            recyclerViewAlerte.setLayoutManager(linearLayoutManager);
            recyclerViewAlerte.setAdapter(listingHomeByItemAdapters);
        }
    }

}
