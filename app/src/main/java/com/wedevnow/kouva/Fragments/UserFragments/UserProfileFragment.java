package com.wedevnow.kouva.Fragments.UserFragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wedevnow.kouva.Adapters.UserAdapters.UserProfileAdapter;
import com.wedevnow.kouva.Extras.SimpleDividerItemDecoration;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.AuthService;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileFragment extends Fragment {


    public UserProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);

        // SHOW USER INFORMATIONS
        if (AuthService.CheckIfUserIsConnected(getContext())) {
            ((TextView)view.findViewById(R.id.nom_user)).setText(AuthService.getUserInfo(getContext(),"userName"));
            ((TextView)view.findViewById(R.id.email_user)).setText(AuthService.getUserInfo(getContext(),"userEmail"));
        } else {
            ((TextView)view.findViewById(R.id.nom_user)).setVisibility(View.GONE);
            ((TextView)view.findViewById(R.id.email_user)).setVisibility(View.GONE);
        }


        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerProfile);
        UserProfileAdapter adapter = new UserProfileAdapter(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        return view;
    }

}
