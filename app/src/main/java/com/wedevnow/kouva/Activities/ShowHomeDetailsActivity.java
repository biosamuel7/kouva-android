package com.wedevnow.kouva.Activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.tabs.TabLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.firebase.firestore.DocumentSnapshot;
import com.wedevnow.kouva.Activities.UserActivities.LoginActivity;
import com.wedevnow.kouva.Adapters.HomeAdapters.ImageSlideAdapter;
import com.wedevnow.kouva.Adapters.HomeAdapters.TabPagerParallaxAdapter;
import com.wedevnow.kouva.Fragments.HomeFragments.AppartementDetailFragment;
import com.wedevnow.kouva.Fragments.HomeFragments.GalleryFragment;
import com.wedevnow.kouva.Fragments.HomeFragments.MagasinDetailFragment;
import com.wedevnow.kouva.Fragments.HomeFragments.StudioDetailFragment;
import com.wedevnow.kouva.Fragments.HomeFragments.VillaDetailFragment;
import com.wedevnow.kouva.Models.Homes.HomeModel;
import com.wedevnow.kouva.Models.Homes.ImageModel;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.AuthService;
import com.wedevnow.kouva.Services.DataBaseService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;

public class ShowHomeDetailsActivity extends AppCompatActivity {
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    ViewPager viewPagerSlide;
    ImageSlideAdapter adapter;
    Handler handler;
    Runnable runnable;
    Timer timer;
    private HomeModel homeToShowDetails;
    private ImageView imgHeader;
    private Button likeHomeBtn;
    private Button visiteBtn;


    private String link;

    private final int MY_PERMISSIONS_REQUEST_PHONE_CALL = 255;

    private String homeID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_show_home_details);
        if (getIntent().getStringExtra("homeID") != null){
             homeID = getIntent().getStringExtra("homeID");
            DataBaseService
                    .homeCollection()
                    .document(homeID)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful() && task.getResult().exists()){
                                homeToShowDetails = task.getResult().toObject(HomeModel.class);
                                homeToShowDetails.setHomeID(homeID);
                                SetUpHeaderAndNavigation(homeToShowDetails);
                            }
                        }
                    });
        } else {
            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                        @Override
                        public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                            // Get deep link from result (may be null if no link is found)
                            Uri deepLink = null;
                            if (pendingDynamicLinkData != null) {
                                deepLink = pendingDynamicLinkData.getLink();
                                Log.v("debug link LAc:", deepLink.toString());
                                homeID = deepLink.getQueryParameter("id");
                                DataBaseService
                                        .homeCollection()
                                        .document(homeID)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful() && task.getResult().exists()){
                                                    homeToShowDetails = task.getResult().toObject(HomeModel.class);
                                                    homeToShowDetails.setHomeID(homeID);
                                                    SetUpHeaderAndNavigation(homeToShowDetails);
                                                }
                                            }
                                        });
                            }

                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("debug error", "getDynamicLink:onFailure", e);
                        }
                    });
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                onBackPressed();
                return true;
            }
            default:{
                return super.onOptionsItemSelected(item);
            }
        }
    }

    private void SetUpHeaderAndNavigation(final HomeModel homeToShowDetails) {


        //homeToShowDetails = getIntent().getParcelableExtra("homeData");

        //mettre l'image principale dans la gallerie
        ArrayList<ImageModel> imageList = new ArrayList<>();
        imageList.add(homeToShowDetails.getImagePrincipale());
        for (ImageModel img : homeToShowDetails.getImageGallerie()) {
            imageList.add(img);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        likeHomeBtn = (Button)findViewById(R.id.likeButton);
        visiteBtn = (Button)findViewById(R.id.visiteBtn);


        if(homeToShowDetails.getLike()){
            switch (homeToShowDetails.getLikeState()){
                case "wish":
                    likeHomeBtn.setBackgroundResource(R.mipmap.likehome_wish_icon);
            }
        }else {
            likeHomeBtn.setBackgroundResource(R.mipmap.unlike);
        }


        likeHomeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HandleLikeHome();
            }
        });

        visiteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HandleHomeVisite();
            }
        });


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        viewPagerSlide = (ViewPager) findViewById(R.id.view_pager2);
        adapter = new ImageSlideAdapter(this,imageList);
        viewPagerSlide.setAdapter(adapter);


        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                int i = viewPagerSlide.getCurrentItem();


                if (i == adapter.images.size()-1){
                    i=0;
                    viewPagerSlide.setCurrentItem(i);
                } else {

                    i++;
                    viewPagerSlide.setCurrentItem(i);
                }

            }
        };

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        },4000 , 4000);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        imgHeader = (ImageView) findViewById(R.id.img_header);

        SetDynamicLink();


        TabPagerParallaxAdapter mAdapter = new TabPagerParallaxAdapter(getSupportFragmentManager());

        switch (homeToShowDetails.getType()){
            case "studio":
                StudioDetailFragment studioDetailFragment = new StudioDetailFragment();
                studioDetailFragment.setHomedata(homeToShowDetails);
                mAdapter.addFragment(studioDetailFragment,"INFO");
                break;
            case "villa":
                VillaDetailFragment villaDetailFragment = new VillaDetailFragment();
                villaDetailFragment.setHomeData(homeToShowDetails);
                mAdapter.addFragment(villaDetailFragment,"INFO");
                break;
            case "appartement":
                AppartementDetailFragment appartementDetailFragment = new AppartementDetailFragment();
                appartementDetailFragment.setHomeData(homeToShowDetails);
                mAdapter.addFragment(appartementDetailFragment, "INFO");
                break;
            case "magasin":
                MagasinDetailFragment magasinDetailFragment = new MagasinDetailFragment();
                magasinDetailFragment.setHomeData(homeToShowDetails);
                mAdapter.addFragment(magasinDetailFragment, "INFO");
            default:break;

        }

        GalleryFragment galleryFragment = new GalleryFragment();
        galleryFragment.setGalleryList(imageList);

        mAdapter.addFragment(galleryFragment, "Gallerie");

        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        // save tabLayoutPosition

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        TextView prixTV = (TextView)findViewById(R.id.nested_price);
        prixTV.setText(homeToShowDetails.getPrix()+" FCFA");



        TextView localityTV = (TextView)findViewById(R.id.nested_location);
            localityTV.setText(homeToShowDetails.getCommune()+" - "+ homeToShowDetails.getQuartier());

        TextView pieceAndtypeTV = (TextView)findViewById(R.id.piece_tv_id);
        switch (homeToShowDetails.getPossession()){
            case "location":
                switch (homeToShowDetails.getType()) {
                    case "studio" :
                        pieceAndtypeTV.setText("Studio à louer");
                        break;
                    case "magasin":
                        pieceAndtypeTV.setText("Magasin à louer");
                        break;
                    default:
                        pieceAndtypeTV.setText(homeToShowDetails.getType()+" "+ homeToShowDetails.getPieces()+" pieces à louer");
                        break;
                }
                break;
            case "vente":
                ((TextView) findViewById(R.id.per_month_tv_id)).setVisibility(View.GONE);
                pieceAndtypeTV.setText(homeToShowDetails.getType()+" "+ homeToShowDetails.getPieces()+" pieces à vendre");
                break;
            default:
                break;
        }

        if (AuthService.CheckIfUserIsConnected(getApplicationContext())){
            DataBaseService.userWishCollection(getApplicationContext()).document(homeID)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful() && task.getResult().exists()){
                                homeToShowDetails.setLike(true);
                                homeToShowDetails.setLikeState("wish");
                                likeHomeBtn.setBackgroundResource(R.mipmap.likehome_wish_icon);
                            }
                        }
                    });
        }

    }

    public void HandleHomeVisite(){
        if(AuthService.CheckIfUserIsConnected(this)){
            visiteConfirmation();
        } else {
            Intent intent; intent =  new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }
    }

    private void callForVisite(){
        Intent intentCall = new Intent(Intent.ACTION_CALL);
        intentCall.setData(Uri.parse("tel:"+homeToShowDetails.getNumero()));
        DataBaseService.setUserVisite(getApplicationContext(),homeToShowDetails);
        startActivity(intentCall);
    }
    public void visiteConfirmation(){
        AlertDialog.Builder builder = new AlertDialog.Builder(ShowHomeDetailsActivity.this);
        builder.setTitle("Visite du bien");
        builder.setMessage("Voulez vous vraiment visiter ce bien ?");
        builder.setCancelable(false);

        builder.setNegativeButton("non", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


            }
        });
        builder.setPositiveButton("oui", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (ContextCompat.checkSelfPermission(ShowHomeDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    // Permission is not granted
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ShowHomeDetailsActivity.this,
                            Manifest.permission.CALL_PHONE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
//                        Snackbar.make(ShowHomeDetailsActivity.this,"let call",Snackbar.LENGTH_LONG).show();
                        ActivityCompat.requestPermissions(ShowHomeDetailsActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_PHONE_CALL
                        );
                    } else {
                        // No explanation needed; request the permission
                        ActivityCompat.requestPermissions(ShowHomeDetailsActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_PHONE_CALL
                        );
//                        callForVisite();
                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }

                } else {
                    callForVisite();
                }

            }
        });

        final AlertDialog alertDialog = builder.create();

        alertDialog.show();


    }

    public void shareLink(){
        Intent sharedAppIntent = new Intent(Intent.ACTION_SEND);
        sharedAppIntent.putExtra(Intent.EXTRA_TEXT, link);
        sharedAppIntent.setType("text/plain");
        this.startActivity(Intent.createChooser(sharedAppIntent, "Share Link"));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_PHONE_CALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    callForVisite();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    public void SetDynamicLink(){
        String description = "";
        switch (homeToShowDetails.getPossession()){
            case "location":
                if (homeToShowDetails.getPieces() == 1){
                    description = "à louer "+homeToShowDetails.getCommune()+" "+homeToShowDetails.getQuartier()+" à "+homeToShowDetails.getPrix();
                } else {
                    description = homeToShowDetails.getPieces()+" pieces à louer "+homeToShowDetails.getCommune()+" "+homeToShowDetails.getQuartier()+" à "+homeToShowDetails.getPrix();
                }
                break;
            case "vente":
                if (homeToShowDetails.getPieces() == 1){
                    description = "à vendre "+homeToShowDetails.getCommune()+" "+homeToShowDetails.getQuartier()+" à "+homeToShowDetails.getPrix();
                } else {
                    description = homeToShowDetails.getPieces()+" pieces à vendre "+homeToShowDetails.getCommune()+" "+homeToShowDetails.getQuartier()+" à "+homeToShowDetails.getPrix();
                }
                break;
            default:break;

        }
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://kouva.net/search/home?id="+homeToShowDetails.getHomeID()))
                .setDomainUriPrefix("https://kouva.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(new  DynamicLink.IosParameters.Builder("com.wedevnow.kouva")
                        .setAppStoreId("1469657479")
                        .setMinimumVersion("1.0.2")
                        .build())
                .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle(homeToShowDetails.getType())
                        .setDescription(description)
                        .setImageUrl(Uri.parse(homeToShowDetails.getImagePrincipale().getDownloadURL()))
                        .build())
                .buildShortDynamicLink()
                .addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()){
                            Log.v("Link shortlink",task.getResult().getShortLink().toString());
                            link = task.getResult().getShortLink().toString();
                        }
                    }
                });
    }

    public void HandleLikeHome(){

        if (AuthService.CheckIfUserIsConnected(this)){
            if (homeToShowDetails.getLike()){
                DataBaseService.userWishCollection(this).document(homeToShowDetails.getHomeID()).delete();
                likeHomeBtn.setBackgroundResource(R.mipmap.unlike);
                homeToShowDetails.setLike(false);
            } else{
                homeToShowDetails.setLike(true);
                homeToShowDetails.setLikeState("wish");
                DataBaseService.setUserLike(getApplicationContext(),homeToShowDetails);
                likeHomeBtn.setBackgroundResource(R.mipmap.likehome_wish_icon);
            }
        }else {
            Intent intent; intent =  new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }
    }

    public void disPlayDateTimeAge(TextView timePublicationTextView){

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date homeDate = homeToShowDetails.getCreatedAt().toDate();
        long second = TimeUnit.MILLISECONDS.toSeconds(new Date().getTime() - homeDate.getTime());
        long hours = TimeUnit.MILLISECONDS.toHours(new Date().getTime() - homeDate.getTime());
        long day = TimeUnit.MILLISECONDS.toDays(new Date().getTime() - homeDate.getTime());
        long month = day > 30 ? day / 30 : 0;

        if (month != 0){
            timePublicationTextView.setText("Il a "+month+" mois");
        } else if (day != 0) {
            timePublicationTextView.setText("Il a "+day+" jours");
        } else {
            timePublicationTextView.setText("Il a "+hours+" heures");
        }

    }


}
