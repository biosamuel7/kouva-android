package com.wedevnow.kouva.Activities;


import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentReference;
import com.wedevnow.kouva.Activities.UserActivities.LoginActivity;
import com.wedevnow.kouva.Adapters.BudgetAdapter;
import com.wedevnow.kouva.Models.Homes.GMSplace;
import com.wedevnow.kouva.Models.Users.SearchBudget;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.AuthService;
import com.wedevnow.kouva.Services.DataBaseService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CreerAlerteActivity extends AppCompatActivity {

    TextView budget;
    private BottomSheetDialog bottomSheetDialog;
    Button localisation;
    ListView listView;

    private Integer AUTOCOMPLETE_REQUEST_CODE = 2;

    private ArrayList<SearchBudget> searchBudgets;
    private String possession, type;
    private  SearchBudget budgetChoice;
    private GMSplace placeChoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.creer_alerte);

        String apiKey = getString(R.string.google_places_api_key);

        Log.v("debod",apiKey);
        // Initialize the SDK
        Places.initialize(getApplicationContext(), apiKey);

        // Create a new Places client instance
        final PlacesClient placesClient = Places.createClient(this);

        localisation = (Button) findViewById(R.id.localisation);

        localisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Set the fields to specify which types of place data to return.
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fields).build(getApplicationContext());
                startActivityForResult(intent,AUTOCOMPLETE_REQUEST_CODE);
            }
        });

        budget = findViewById(R.id.search_budget);

        searchBudgets = new ArrayList<>();

        for (int i = 0; i < 10; i++){
            Integer min = i * 50000;
            Integer max = (i + 1) * 50000;
            searchBudgets.add(new SearchBudget(min,max));
        }

        for (SearchBudget searchBudget : searchBudgets){
            Log.v("budget",searchBudget.getMin_max());
        }

        bottomSheetDialog = new BottomSheetDialog(this);
        View bottomsheetdialogview = getLayoutInflater().inflate(R.layout.bottom_sheet_budget, null);
        listView = (ListView) bottomsheetdialogview.findViewById(R.id.listView);
        bottomSheetDialog.setContentView(bottomsheetdialogview);
//        final RadioButton radioButton = bottomsheetdialogview.findViewById(R.id.radioBtn1);

        BudgetAdapter adapter2 = new BudgetAdapter(getApplicationContext(),searchBudgets);
        listView.setAdapter(adapter2);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SearchBudget searchBudget = searchBudgets.get(position);
                budgetChoice = searchBudget;
                budget.setText(searchBudget.getMin_max());
                bottomSheetDialog.dismiss();
            }
        });

        if (getIntent().getStringExtra("type") != null){
             possession = getIntent().getStringExtra("possession");
             type = getIntent().getStringExtra("type");
            Log.v("DEB",possession);
            Log.v("DEB",type);
        }


       //BudgetAlerteAdapter budgetAlerteAdapter = new BudgetAlerteAdapter(this, android.R.layout.simple_list_item_single_choice, budget_alerte);
       //listView.setAdapter(budgetAlerteAdapter);

       budget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // budget.setText(radioButton.getText());
                bottomSheetDialog.show();
            }
        });

        ((Button) findViewById(R.id.create_alerte_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.v("auth",AuthService.CheckIfUserIsConnected(getApplicationContext()).toString());
                if (!AuthService.CheckIfUserIsConnected(getApplicationContext())) {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    return;
                }

                if (budgetChoice == null || placeChoice == null){
                    Snackbar.make(v,"Remplir tous les champs",Snackbar.LENGTH_LONG).show();
                    return;
                }

                Log.v("budget",budgetChoice.getMin_max());
                Log.v("budget",possession);
                Log.v("budget",type);
                Log.v("budget",placeChoice.getLongitude().toString());
                Log.v("budget",placeChoice.getLatitude().toString());
                Log.v("budget",placeChoice.getPlace_id());

                Map<String, Object> budget = new HashMap<>();
                budget.put("min",budgetChoice.getMin());
                budget.put("max",budgetChoice.getMax());

                Map<String, Object> searchAlerte = new HashMap<>();
                searchAlerte.put("budget",budget);
                searchAlerte.put("possession",possession);
                searchAlerte.put("type",type);
                searchAlerte.put("places",placeChoice);

                final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progression_id);
                progressBar.setVisibility(View.VISIBLE);
                final Button button = (Button) v;
                button.setText("");
                DataBaseService.userSearchCollection(getApplicationContext())
                        .add(searchAlerte)
                        .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            finish();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        button.setText("Rechercher");
                    }
                });

            }
        });
/*
       listView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               bottomSheetDialog.dismiss();
           }
       });

/*

        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });

*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i("focus", "Place: " + place.getName() + ", " + place.getId());
                localisation.setText(place.getName());
                placeChoice = new GMSplace(place.getLatLng().longitude,place.getLatLng().latitude,place.getAddress(),place.getId(),place.getName());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("focus", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
