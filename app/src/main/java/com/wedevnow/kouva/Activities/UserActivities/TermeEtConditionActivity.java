package com.wedevnow.kouva.Activities.UserActivities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.Constants;

public class TermeEtConditionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terme_et_condition);

        WebView webView = (WebView)findViewById(R.id.webWiew_id);
        webView.getSettings().getJavaScriptEnabled();
        webView.loadUrl(Constants.TERMS_URL);

    }
}
