package com.wedevnow.kouva.Activities;

import android.content.Intent;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.wedevnow.kouva.Fragments.FavorisFragment;
import com.wedevnow.kouva.Fragments.MainFragment;
import com.wedevnow.kouva.Fragments.AlertesFragment;
import com.wedevnow.kouva.Fragments.UserFragments.UserProfileFragment;
import com.wedevnow.kouva.R;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;

    public MainFragment mainFragment ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fabric.with(this, new Crashlytics());

        if(mainFragment == null){
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new MainFragment()).commit();
        }

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setBottom(1);
        setUpBottomNavigationView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setUpBottomNavigationView();
    }
    public void setUpBottomNavigationView(){

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected( MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.id_recherche:
                        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new MainFragment()).commit();
                        item.setChecked(true);
                        break;

                    case R.id.id_wishlist:
                        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new FavorisFragment()).commit();
                        item.setChecked(true);
                        break;

                    case R.id.id_alerte:
                        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new AlertesFragment()).commit();
                        item.setChecked(true);
                        break;

                    case R.id.id_user_profil:
                        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, new UserProfileFragment()).commit();
                        item.setChecked(true);
                        break;
                }
                return true;

            }
        });
    }

    public BottomNavigationView getBottomNavigationView() {
        return bottomNavigationView;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 0){
            bottomNavigationView.setSelectedItemId(R.id.id_user_profil);
        }
    }
}
