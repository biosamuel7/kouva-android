package com.wedevnow.kouva.Activities;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.wedevnow.kouva.Fragments.AuthFragments.AddUserNumberPhoneFragment;
import com.wedevnow.kouva.Fragments.AuthFragments.RegisterFragment;
import com.wedevnow.kouva.Fragments.AuthFragments.VerificationUserNumberFragment;
import com.wedevnow.kouva.R;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final ViewPager mPager = (ViewPager) findViewById(R.id.viewPager);
        mPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0 :
                        RegisterFragment registerFragment = new RegisterFragment();
                        registerFragment.setViewPager(mPager);
                        return  registerFragment;
                    case 1 :
                        AddUserNumberPhoneFragment addUserNumberPhoneFragment = new AddUserNumberPhoneFragment();
                        addUserNumberPhoneFragment.setResultCode(1);
                        addUserNumberPhoneFragment.setViewPager(mPager);
                        return addUserNumberPhoneFragment;
                    case 2 :
                        VerificationUserNumberFragment verificationNumberFragment = new VerificationUserNumberFragment();
                        verificationNumberFragment.setResultCode(1);
                        verificationNumberFragment.setViewPager(mPager);
                        return verificationNumberFragment;

                    default:return null;

                }
            }

            @Override
            public int getCount() {
                return 3;
            }
        });
    }
}
