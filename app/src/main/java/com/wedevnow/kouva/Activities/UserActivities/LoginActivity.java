package com.wedevnow.kouva.Activities.UserActivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.wedevnow.kouva.Activities.RegisterActivity;
import com.wedevnow.kouva.Fragments.AuthFragments.AddUserNumberPhoneFragment;
import com.wedevnow.kouva.Fragments.AuthFragments.VerificationUserNumberFragment;
import com.wedevnow.kouva.Models.Users.UserModel;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.AuthService;
import com.wedevnow.kouva.Services.DataBaseService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity {

    private Button mSeConnnecter , mEnregister;
    private EditText mEmailEditText,mPasswordEditText;
    private TextView mDisplaypassword , termCond;
    private EditText  email , password;
    private ViewPager viewPager;
    private ProgressDialog dialog;

    private CallbackManager mCallbackManager;

    private GoogleSignInClient mGoogleSignInClient;

    private Boolean checkstate = true ;

    int RC_SIGN_IN = 1994;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Fabric.with(this, new Crashlytics());

        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.wedevnow.kouva",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    Log.w("LOG TOKEN", "getInstanceId failed", task.getException());
                    return;
                }

                // Get new Instance ID token
                String token = task.getResult().getToken();

                AuthService.saveUserInfo(getApplicationContext(),"token",token);
                // Log and toast
                //String msg = getString("dd", token);
                Log.v("TOKEN",token);
                //Toast.makeText(MainActivity.this, token.toString(), Toast.LENGTH_SHORT).show();
            }


        });


        // facebook login
        Button facebookBtn = (Button) findViewById(R.id.facebook_btn);
        mCallbackManager = CallbackManager.Factory.create();
        facebookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logOut();
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile","email"));
                LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        dialog.show();
                        Log.d("debug", "facebook:onSuccess:" + loginResult);
                        AuthCredential credential = FacebookAuthProvider.getCredential(loginResult.getAccessToken().getToken());
                        signInUserWithCredential(credential);
                    }

                    @Override
                    public void onCancel() {
                        dialog.cancel();
                        Log.d("debug", "facebook:onCancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d("debug", "facebook:onError", error);
                    }
                });
            }
        });


        // google signin
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.v("debug","user log out successful");
                    }
                });

        Button googleBtn = (Button) findViewById(R.id.google_btn);
        googleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                dialog.show();
            }
        });



        // EMAIL AND PASSWORD CONNEXION
        email = (EditText) findViewById(R.id.id_email);
        password = (EditText) findViewById(R.id.id_password);
        mSeConnnecter = (Button) findViewById(R.id.buttonSeConnecter);
        mEnregister = (Button) findViewById(R.id.buttonEnregistrer);
        mEmailEditText = (EditText) findViewById(R.id.id_email);
        mPasswordEditText = (EditText) findViewById(R.id.id_password);
        final ProgressBar loginProgression = (ProgressBar) findViewById(R.id.loginProgression);

        email.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        /// display password
        mDisplaypassword = (TextView) findViewById(R.id.afficher_password);
        mDisplaypassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkstate == true){
                    mDisplaypassword.setText("Masquer");
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    checkstate = false;
                }

                else {
                    mDisplaypassword.setText("Afficher");
                    password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    checkstate = true;
                }
            }
        });

        mSeConnnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mSeConnnecter.setText(" ");
                loginProgression.setVisibility(View.VISIBLE);

                if(mEmailEditText.getText().length() == 0){
                    Snackbar.make(view,"Saisir son email",Snackbar.LENGTH_LONG)
                            .setAction("Action",null).show();
                }
                if(mPasswordEditText.getText().length() == 0){
                    Snackbar.make(view,"Saisir son password",Snackbar.LENGTH_LONG)
                            .setAction("Action",null).show();
                }

                if(mEmailEditText.getText().length() != 0 && mPasswordEditText.getText().length() != 0){
                    AuthService.auth().signInWithEmailAndPassword(mEmailEditText.getText().toString(),mPasswordEditText.getText().toString())
                            .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull final Task<AuthResult> task) {
                                    if(task.isSuccessful()){

                                        AuthService.saveUserInfo(getApplicationContext(),"userID",task.getResult().getUser().getUid());
                                        DataBaseService.userCollection().document(task.getResult().getUser().getUid())
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> taskSnapshot) {
                                                        if (task.isSuccessful()){
                                                            DocumentSnapshot documentSnapshot = taskSnapshot.getResult();
                                                            if (documentSnapshot.exists()){
                                                                //set token for user
                                                                String token = AuthService.getUserInfo(getApplicationContext(),"token");
                                                                DataBaseService.userDocument(getApplicationContext()).update("fcm_token",token);

                                                                UserModel userModel = documentSnapshot.toObject(UserModel.class);
                                                                AuthService.saveUserInfo(getApplicationContext(),"userName",userModel.getPrenom()+" "+userModel.getNom());
                                                                AuthService.saveUserInfo(getApplicationContext(),"userEmail",userModel.getEmail());
                                                                Intent intent = new Intent();
                                                                loginProgression.setVisibility(View.GONE);
                                                                mSeConnnecter.setText("Connexion");
                                                                setResult(0,intent);
                                                                finish();
                                                            }
                                                        }
                                                    }
                                                });

                                    } else {
                                        loginProgression.setVisibility(View.GONE);
                                        mSeConnnecter.setText("Connexion");
                                        Log.v("debug code erro",String.valueOf(task.getException().hashCode()));
                                        Snackbar.make(view,"Password ou Email incorrect",Snackbar.LENGTH_LONG)
                                                .setAction("Action",null).show();
                                    }
                                }
                            });
                }


            }
        });
        mEnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivityForResult(intent,1);
            }
        });

        // termes et conditions clickable
        termCond = (TextView) findViewById(R.id.termcond);
        setTermAndCondition();

        viewPager = (ViewPager) findViewById(R.id.login_viewPager);
        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int postion) {
                switch (postion) {
                    case 0:
                        AddUserNumberPhoneFragment addUserNumberPhoneFragment = new AddUserNumberPhoneFragment();
                        addUserNumberPhoneFragment.setResultCode(0);
                        addUserNumberPhoneFragment.setViewPager(viewPager);
                        return addUserNumberPhoneFragment;
                    case 1:
                        VerificationUserNumberFragment verificationNumberFragment = new VerificationUserNumberFragment();
                        verificationNumberFragment.setViewPager(viewPager);
                        verificationNumberFragment.setResultCode(0);
                        return  verificationNumberFragment;

                    default: return null;
                }
            }

            @Override
            public int getCount() {
                return 2;
            }
        });
    }

    private void setTermAndCondition() {
        String Text = "En appuyant sur les boutons de connexion ci-dessus , j'accepte les Termes et conditions d'utilisation de Kouva";
        SpannableString spannableString = new SpannableString(Text);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intentTerme = new Intent(LoginActivity.this, TermeEtConditionActivity.class);
                startActivity(intentTerme);
            }
        };

        spannableString.setSpan(clickableSpan, 67 , 87 , Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        termCond.setText(spannableString);
        termCond.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void signInUserWithCredential(AuthCredential credential) {
        AuthService.auth().signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull final Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.v("debug uid",task.getResult().getUser().getUid());
                            final FirebaseUser user = task.getResult().getUser();
                            AuthService.saveUserInfo(getApplicationContext(),"userID",user.getUid());
                            AuthService.saveUserInfo(getApplicationContext(),"userName",user.getDisplayName());
                            AuthService.saveUserInfo(getApplicationContext(),"userEmail",user.getEmail());
                            //retreive user to if is already create before
                            //Intent intent = new Intent();
                            //setResult(0,intent);
                            //finish();
                            //check if user number is store in database befor to show login viewPager


                            final String token = AuthService.getUserInfo(getApplicationContext(),"token");
                            Log.v("token login",token);
                            DataBaseService.userDocument(getApplicationContext()).update("fcm_token",token);

                            DataBaseService.userCollection().document(user.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()){
                                        DocumentSnapshot documentSnapshot = task.getResult();
                                        if (documentSnapshot.exists()){
                                            UserModel userModel = documentSnapshot.toObject(UserModel.class);
                                            if (userModel.getNumero() != null){
                                                Intent intent = new Intent();
                                                setResult(0,intent);
                                                finish();
                                            } else {
                                                dialog.cancel();
                                                viewPager.setVisibility(View.VISIBLE);
                                            }
                                        } else {
                                            DataBaseService.userCollection().document(user.getUid()).set(new UserModel(user.getDisplayName(),"",user.getEmail(),token));
                                            dialog.cancel();
                                            viewPager.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }
                            });
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.v("debug erro",e.getLocalizedMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v("debug resultCode:",String.valueOf(resultCode));
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode,resultCode,data);
        if(resultCode == 1){
            finish();
        }

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                Log.v("debug","succ");
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(),null);
                signInUserWithCredential(credential);
            } catch (ApiException e) {
                dialog.cancel();
                // Google Sign In failed, update UI appropriately
                Log.w("debug", "Google sign in failed", e);
                // ...
            }
        }
    }

    @Override
    protected void onDestroy() {
        dialog.dismiss();
        super.onDestroy();
    }

    /**
     * hide keyboard method
     */
    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        AuthService.logout(getApplicationContext());
        finish();
    }
}
