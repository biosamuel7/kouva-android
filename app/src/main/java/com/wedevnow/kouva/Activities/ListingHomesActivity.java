package com.wedevnow.kouva.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.crashlytics.android.Crashlytics;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.wedevnow.kouva.Adapters.ListingHomeByItemAdapter;
import com.wedevnow.kouva.Adapters.ListingHomesBySectionAdapter;
import com.wedevnow.kouva.Adapters.NombreDePiecesAdapter;
import com.wedevnow.kouva.Models.Homes.HomeModel;
import com.wedevnow.kouva.Models.Homes.ListingHomeModel;
import com.wedevnow.kouva.Models.Homes.NombreDePieceModel;
import com.wedevnow.kouva.R;
import com.wedevnow.kouva.Services.DataBaseService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class ListingHomesActivity extends AppCompatActivity {

    private ShimmerFrameLayout mShimmerViewContainer;
    public RecyclerView mRecyclerView_listings;
    RecyclerView mRecyclerView_nbpieces;



    LinearLayoutManager layoutManager;

    ListingHomesBySectionAdapter listingHomesBySectionAdapter;
    NombreDePiecesAdapter nombreDePiecesAdapter;

    ListingHomeByItemAdapter listingHomeAdapter;

    EditText searchEdit;

    String possession, type;

    Button alerteBtn;
   // FloatingTextButton alerteBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_homes);




        Fabric.with(this, new Crashlytics());

        if (getIntent().getStringExtra("type") != null){
            possession = getIntent().getStringExtra("possession");
            type = getIntent().getStringExtra("type");
            new RetrieveHomeDataQueryTask().execute();
        }

        alerteBtn = (Button) findViewById(R.id.btn_alert);
        alerteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().getStringExtra("type") != null){
                    possession = getIntent().getStringExtra("possession");
                    type = getIntent().getStringExtra("type");
                    Intent intent = new Intent(getApplicationContext(), CreerAlerteActivity.class);
                    intent.putExtra("type",type);
                    intent.putExtra("possession",possession);
                    startActivity(intent);
                }
           /*     CreerAlerteActivity creerAlerteFragment = new CreerAlerteActivity();
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, creerAlerteFragment)
                        .addToBackStack(ListingHomesActivity.class.getSimpleName())
                        .commit(); */
            }
        });

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        searchEdit = (EditText) findViewById(R.id.search_id_nb);

        searchEdit.setInputType(InputType.TYPE_TEXT_VARIATION_URI);

        searchEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });


        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView_listings = (RecyclerView) findViewById(R.id.recyclerview_listings_home_by_section);

//        mRecyclerView_nbpieces = (RecyclerView) findViewById(R.id.recyclerview_nombre_de_pieces);

        mRecyclerView_listings.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int positionSection = layoutManager.findFirstVisibleItemPosition();
//                if (layoutManager.findLastCompletelyVisibleItemPosition() == )
//                nombreDePiecesAdapter.selectePiece(positionSection);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

    }


    public class RetrieveHomeDataQueryTask2 extends AsyncTask<Void,Void, ArrayList<ListingHomeModel>> {

        ArrayList<ListingHomeModel> dataHomesListing = new ArrayList<>();

        ArrayList<NombreDePieceModel> pieceData = new ArrayList<>();
        @Override
        protected ArrayList<ListingHomeModel> doInBackground(Void... voids) {
            final ArrayList<HomeModel> listHome = new ArrayList<>();
            final Map<Integer,ArrayList<HomeModel>> locationData = new HashMap<>();
            DataBaseService.homeCollection()
                    .whereEqualTo("possession",possession)
                    .whereEqualTo("type",type)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    HomeModel home = document.toObject(HomeModel.class);
                                    home.setHomeID(document.getId());

                                    //retreive only home available
                                    if (home.getDisponibilite()) {
                                        mShimmerViewContainer.setVisibility(View.GONE);
                                        listHome.add(home);
                                    }
                                }//end loop for

                                for (HomeModel item : listHome){
                                    locationData.put(item.getPieces(),null);
                                }

                                for(int column : locationData.keySet()){
                                    ArrayList<HomeModel> homes = new ArrayList<>();
                                    for (HomeModel home : listHome){
                                        if(home.getPieces() == column){
                                            homes.add(home);
                                            locationData.put(column,homes);
                                        }
                                    }
                                }

                                for(int column: locationData.keySet()){

                                    dataHomesListing.add(new ListingHomeModel(column,locationData.get(column)));
                                }

                                Boolean conditiontTie = true;
                                while (conditiontTie){
                                    conditiontTie = false;
                                    for (int i =0 ; i < dataHomesListing.size() ; i++){
                                        if (i < dataHomesListing.size()-1){
                                            ListingHomeModel h1 = dataHomesListing.get(i);
                                            ListingHomeModel h2  = dataHomesListing.get(i+1);
                                            if (h1.getColumn() > h2.getColumn()){
                                                ListingHomeModel home = h1;
                                                dataHomesListing.set(i,h2);
                                                dataHomesListing.set(i+1,home);
                                                conditiontTie = true;
                                            }
                                        }
                                    }
                                }

                                for (int i = 0; i < dataHomesListing.size(); i++) {

                                    Boolean checkUp = true;

                                    ArrayList<HomeModel> homes = dataHomesListing.get(i).getHomes();

                                    while (checkUp) {
                                        checkUp = false;
                                        for (int j = 0; j < homes.size(); j++) {
                                            if (j < homes.size()-1) {
                                                HomeModel home = homes.get(j);
                                                HomeModel home2 = homes.get(j+1);
                                                Integer prixToInt1 = Integer.parseInt(home.getPrix().replace(" ",""));
                                                Integer prixToInt2 = Integer.parseInt(home2.getPrix().replace(" ",""));
                                                Log.v("TRIE",String.valueOf(prixToInt1));
                                                if(prixToInt1 > prixToInt2) {
                                                    HomeModel homeToChange = home;
                                                    homes.set(j,home2);
                                                    homes.set(j+1,homeToChange);
                                                    checkUp = true;
                                                }
                                            }
                                        }
                                    }
                                }


                                for (ListingHomeModel home : dataHomesListing){
                                    pieceData.add(new NombreDePieceModel(home.getColumn(),false));
                                }

                                if (listHome.size() > 0) pieceData.get(0).setSelected(true);

                                mRecyclerView_nbpieces.invalidate();
                                mRecyclerView_listings.invalidate();
                                nombreDePiecesAdapter.notifyDataSetChanged();
                                listingHomesBySectionAdapter.notifyDataSetChanged();

                            }
                        }
                    });

            return dataHomesListing;
        }

        @Override
        protected void onPostExecute(ArrayList<ListingHomeModel> dataHomes) {

            listingHomesBySectionAdapter = new ListingHomesBySectionAdapter(ListingHomesActivity.this,dataHomes);
            mRecyclerView_listings.setAdapter(listingHomesBySectionAdapter);
            mRecyclerView_listings.setHasFixedSize(true);
            mRecyclerView_listings.setLayoutManager(layoutManager);

            nombreDePiecesAdapter = new NombreDePiecesAdapter(ListingHomesActivity.this , pieceData);
            mRecyclerView_nbpieces.setAdapter(nombreDePiecesAdapter);
            mRecyclerView_nbpieces.setHasFixedSize(true);
            mRecyclerView_nbpieces.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL   , false));
        }
    }


    public class RetrieveHomeDataQueryTask extends AsyncTask<Void,Void, ArrayList<HomeModel>> {

        @Override
        protected ArrayList<HomeModel> doInBackground(Void... voids) {
            final ArrayList<HomeModel> listHome = new ArrayList<>();
            DataBaseService.homeCollection()
                    .whereEqualTo("possession",possession)
                    .whereEqualTo("type",type)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        HomeModel home = document.toObject(HomeModel.class);
                                        home.setHomeID(document.getId());

                                        //retreive only home available
                                        if (home.getDisponibilite()) {
                                            mShimmerViewContainer.setVisibility(View.GONE);
                                            listHome.add(home);
                                        }
                                    }//end loop for

                                    mRecyclerView_listings.invalidate();
                                    listingHomeAdapter.notifyDataSetChanged();

                            }
                        }
                    });

            return listHome;
        }

        @Override
        protected void onPostExecute(ArrayList<HomeModel> dataHomes) {

            listingHomeAdapter = new ListingHomeByItemAdapter(ListingHomesActivity.this,dataHomes);
            mRecyclerView_listings.setAdapter(listingHomeAdapter);
            mRecyclerView_listings.setHasFixedSize(true);
            mRecyclerView_listings.setLayoutManager(layoutManager);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }



    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
