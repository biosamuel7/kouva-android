package com.wedevnow.kouva.Datas;

import com.wedevnow.kouva.Models.TypeDeBienModel;
import com.wedevnow.kouva.R;

import java.util.ArrayList;

public class DataTypeDeBienAchat {
    public static ArrayList<TypeDeBienModel> getData() {

        ArrayList<TypeDeBienModel> data = new ArrayList<>();


        String[] Categories = {
                "APPARTEMENT",
                "VILLA"
        };


        for (int i = 0 ; i <Categories.length ; i++)
        {
            TypeDeBienModel current = new TypeDeBienModel();
            current.setTypeDebien(Categories[i]);
            current.setIcon(R.mipmap.newspaper);
            data.add(current);
        }
        data.get(0).setSelected(true);
        return data;
    }
}
